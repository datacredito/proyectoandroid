import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "@nativescript/angular";

import { AyudaRoutingModule } from "./ayuda-routing.module";
import { AyudaComponent } from "./ayuda.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        AyudaRoutingModule
    ],
    declarations: [
        AyudaComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AyudaModule { }
