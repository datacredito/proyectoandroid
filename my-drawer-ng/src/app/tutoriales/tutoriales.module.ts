import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "@nativescript/angular";

import { TutorialesRoutingModule } from "./tutoriales-routing.module";
import { TutorialesComponent } from "./tutoriales.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        TutorialesRoutingModule
    ],
    declarations: [
        TutorialesComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class TutorialesModule { }
